﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace AutoFriend.Domain
{
    public class FriendlyWebManager
    {
        private string _input;
        private IUpdatable _update;
        public FriendlyWebManager(string input, IUpdatable update)
        {
            _input = input;
            _update = update;
        }

        public string ReadWebsite()
        {
            WebRequest request = WebRequest.Create(_input);
            // If required by the server, set the credentials.  
            request.Credentials = CredentialCache.DefaultCredentials;
            // Get the response.  
            WebResponse response = request.GetResponse();
            // Get the stream containing content returned by the server.  
            Stream dataStream = response.GetResponseStream();
            // Open the stream using a StreamReader for easy access.  
            StreamReader reader = new StreamReader(dataStream);
            // Read the content.  
            string responseFromServer = reader.ReadToEnd();
            // Display the content.  
            //Console.WriteLine(responseFromServer);
            _update.Report(responseFromServer);

            return responseFromServer;
        }

        public static FriendlyWebManager New(string input, IUpdatable update)
        {
            return new FriendlyWebManager(input, update);
        }
    }
}
