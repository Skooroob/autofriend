﻿using AutoFriend.Persistence;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Troschuetz.Random.Generators;
using Troschuetz.Random;
using AutoFriend.DTO.Grammar;
using AutoFriend.DTO.Memory;

namespace AutoFriend.Domain.Speech
{
    public class BasicSpeech
    {
        private IMemorable _memory;
        private string _output = string.Empty;
        private XorShift128Generator _generator;
        private int _verbosity;
        private List<string> _usedRelations;
        private List<string> _usedClauses;
        private int _relationRecursions = 0;
        private int _clauseRecursions = 0;

        public BasicSpeech(IMemorable memory)
        {
            _memory = memory;
            _generator = new XorShift128Generator();
            _verbosity = (int)_generator.NextUInt(3, 10);
            _usedRelations = new List<string>();
            _usedClauses = new List<string>();
        }

        public string TalkAbout(string name)
        {
            if (!_memory.HasConcept(name))
                return $"I know nothing of {name}";

            var concept = _memory.GetConcept(name);

            var intuition = ChooseIntuition(concept);

            var relation = ChooseRelation(concept);
            _usedRelations.Add(relation);

            _output = $"{_output} {intuition.Text}.";

            for (int i = 0; i < _verbosity; i++)
            {
                _relationRecursions = 0;
                _clauseRecursions = 0;
                var relatedConcept = _memory.GetConcept(relation);
                intuition = ChooseIntuition(relatedConcept);
                relation = ChooseRelation(relatedConcept);
                _output = $"{_output} {intuition.Text}.";
            }


            return _output;
        }

        private Clause ChooseIntuition(Concept concept)
        {
            _clauseRecursions++;
            var index = _generator.NextUInt(0, (uint)concept.Intuitions.Count);
            var intuition = concept.Intuitions[(int)index];
            if (_usedClauses.Contains(intuition.Text) && _clauseRecursions < 10)
            {
                return ChooseIntuition(concept);
            }

            return intuition;
        }

        private string ChooseRelation(Concept concept)
        {
            _relationRecursions++;
            var index = _generator.NextUInt(0, (uint)concept.Relations.Count);
            var r = concept.Relations[(int)index];
            if (_usedRelations.Contains(r) && _relationRecursions < 10)
            {
                return ChooseRelation(concept);
            }

            return concept.Relations[(int)index];
        }

        public string TalkAboutAnything()
        {

            return null;
        }
        

    }
}
