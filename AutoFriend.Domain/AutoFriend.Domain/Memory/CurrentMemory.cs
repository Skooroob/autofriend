﻿using AutoFriend.DTO.Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutoFriend.Domain.Memory
{
    public static class CurrentMemory
    {
        public static Dictionary<string, Concept> Concepts;
        public static bool Initialized = false;
        
        public static void InitiateBasicConcepts()
        {
            if (Initialized)
                return;

            Concepts = new Dictionary<string, Concept>()
            {
                { "GOD", new Concept("GOD")},
                { "KING", new Concept("KING")},
                { "COUNTRY", new Concept("COUNTRY")},
                { "FAMILY", new Concept("FAMILY")},
            };

            Initialized = true;
        }
    }
}
