﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using AutoFriend.Persistence;
using AutoFriend.DTO;
using AutoFriend.DTO.Memory;
using AutoFriend.DTO.Grammar;

using StringComparison;
using AutoFriend.Domain.Grammar;

namespace AutoFriend.Domain.Memory
{
    public class MockMemory : IMemorable
    {
        public MockMemory()
        {
            CurrentMemory.InitiateBasicConcepts();
        }

        public void AddConcept(Concept concept)
        {
            CurrentMemory.Concepts.Add(concept.Name, concept);
        }

        public bool HasConcept(string name)
        {
            if (CurrentMemory.Concepts.ContainsKey(name.ToUpper()))
                return true;
            else
                return false;
        }

        public void AddClauseToConcept(string name, Clause clause)
        {
            
            var concept = CurrentMemory.Concepts[name.ToUpper()];

            var contains = false;

            Parallel.ForEach(concept.Intuitions, c =>
            {
                if (contains)
                    return;

                if (c.Text.SorensenDiceDistance(clause.Text) > 0.75f)
                {
                    contains = true;
                    return;
                }
                else if (c.Terms.TermComparison(clause.Terms) >= 0.6f)
                {
                    contains = true;
                    return;
                }
            });
            
            
            if (contains)
                return;

            concept.Intuitions.Add(clause);

            foreach (var term in clause.Terms)
            {
                if (term.ToUpper() == name.ToUpper())
                    continue;
                if (term.IsGrammatical())
                    continue;

                if (!concept.Relations.Contains(term.ToUpper()))
                {
                    concept.Relations.Add(term.ToUpper());
                }
            }
        }

        public Concept GetConcept(string name)
        {
            return CurrentMemory.Concepts[name.ToUpper()];
        }
    }
}
