﻿
using AutoFriend.DTO;
using HtmlAgilityPack;

namespace AutoFriend.Domain
{
    public class FriendlyHtmlParser
    {
        private string _input;
        private IUpdatable _update;
        public FriendlyHtmlParser(string input, IUpdatable update)
        {
            _input = input;
            _update = update;
        }

        public string[] ReadLines()
        {
            var doc = new HtmlDocument();
            doc.LoadHtml(_input);
            var text = doc.DocumentNode.InnerText;
            _update.Report(text);
            return text.SplitByNewLine();
        }


        public static FriendlyHtmlParser New(string input, IUpdatable update)
        {
            return new FriendlyHtmlParser(input, update);
        }
    }
}
