﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutoFriend.Domain.Grammar
{
    public static class Syntax
    {
        public static Dictionary<string, int> Articles = new Dictionary<string, int>()
        {
            {"THE",0 },
            {"A",0 },
        };

        public static Dictionary<string, int> Punctuation = new Dictionary<string, int>()
        {
            {",",0 },
            {";",0 },
            {":",0 },
            {".",0 },
        };


        public static Dictionary<string, int> Conjuctions = new Dictionary<string, int>()
        {
            {"OR",0 },
            {"AND",0 },
        };

        public static bool IsGrammatical(this string word)
        {
            if (Syntax.Punctuation.ContainsKey(word.ToUpper()))
                return true;
            else if (Syntax.Articles.ContainsKey(word.ToUpper()))
                return true;
            else if (Syntax.Conjuctions.ContainsKey(word.ToUpper()))
                return true;

            return false;
        }
    }
}
