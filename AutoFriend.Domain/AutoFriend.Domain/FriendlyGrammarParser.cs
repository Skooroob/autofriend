﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using AutoFriend.Domain.Grammar;
using AutoFriend.DTO;
using AutoFriend.DTO.Grammar;

namespace AutoFriend.Domain
{
    public class FriendlyGrammarParser
    {
        private string[] _lines;
        private List<Sentence> _sentences;

        public FriendlyGrammarParser(string[] lines)
        {
            _lines = lines;
            _sentences = new List<Sentence>();
        }

        public List<Sentence> CreateSentences()
        {
            DivideLinesIntoSentences();
            SetClauses();
            return _sentences;
        }

        private void DivideLinesIntoSentences()
        {
            var words = new List<string>();

            for (int i = 0; i < _lines.Length; i++)
            {
                var terms = _lines[i].SplitBySpace();
                foreach (var term in terms)
                {
                    if (term == ".")
                    {
                        _sentences.Add(new Sentence(words));
                        words = new List<string>();
                    }
                    else if (term == "?")
                    {
                        _sentences.Add(new Sentence(words, true));
                        words = new List<string>();
                    }
                    else
                        words.Add(term);
                }
            }
        }

        private void SetClauses()
        {
            foreach (var sentence in _sentences)
            {
                var words = new List<string>();
                var clauses = new List<Clause>();
                var terms = sentence.Terms;
                foreach (var term in terms)
                {
                    if (ClauseDivisions.Contains(term))
                    {
                        clauses.Add(new Clause(string.Join(" ", words)));
                        words = new List<string>();
                    }
                    else
                    {
                        words.Add(term);
                    }
                }

                if (words.Count > 0)
                {
                    clauses.Add(new Clause(string.Join(" ", words)));
                }

                sentence.Clauses = clauses.ToArray();
            }
        }

        public static FriendlyGrammarParser New(string[] lines)
        {
            return new FriendlyGrammarParser(lines);
        }

        private List<string> ClauseDivisions = new List<string>()
        {
            ";", ":", ",", "-"
        };

    }
}
