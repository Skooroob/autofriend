﻿using AutoFriend.Domain.Grammar;
using AutoFriend.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutoFriend.Domain
{
    public class FriendlySorter
    {
        public List<string> _lines { get; set; }
        public FriendlySorter(string[] lines)
        {
            _lines = lines.ToList();
        }

        public List<string> Sort()
        {
            Clean();
            FixPuncuation();
            return _lines;
        }

        private void FixPuncuation()
        {
            for (int i = 0; i < _lines.Count; i++)
            {
                if (TryFixCamelCase(_lines[i], out string result))
                {
                    _lines[i] = result;
                }
            }
        }

        public bool TryFixCamelCase(string line, out string result)
        {
            var didContainCamelCase = false;
            var terms = line.SplitBySpace();
            var newTerms = new List<string>();

            foreach (var term in terms)
            {
                if (UndoCamelCase(term, out List<string> words))
                {
                    didContainCamelCase = true;
                    newTerms.AddRange(words);
                }
                else
                {
                    newTerms.Add(term);
                }
            }

            if (didContainCamelCase)
            {
                result = string.Join(" ", newTerms);
                return true;
            }
            
            result = null;
            return false;
        }

        private bool UndoCamelCase(string term, out List<string> split)
        {
            var words = new List<string>();
            var start = 0;
            for (int i = 0; i < term.Length; i++)
            {
                if (i == term.Length - 1)
                    break;

                var currentChar = term[i].ToString();
                var nextChar = term[i + 1].ToString();

                if (!IsUppercase(currentChar) && IsUppercase(nextChar))
                {
                    //we want a string constituted from start to i
                    var substr = term.Substring(start, i - start + 1);
                    words.Add(substr);
                    start = i + 1;
                }
                
            }

            if (start != 0)
            {
                var substr = term.Substring(start, term.Length - start);
                words.Add(substr);
                split = words;
                return true;
            }


            split = null;
            return false;
        }

        private bool IsUppercase(string term)
        {
            if (term == term.ToUpper())
                return true;
            else
                return false;
        }

        private void Clean()
        {
            for (int i = 0; i < _lines.Count; i++)
            {
                var line = _lines[i];

                if (LineIsCode(_lines[i]))
                {
                    _lines.RemoveAt(i);
                    i--;
                }
                else if (TryCleanLine(_lines[i], out string result))
                {
                    _lines[i] = result;
                }
                else
                {
                    _lines.RemoveAt(i);
                    i--;
                }
            }
        }
        
        private bool TryCleanLine(string line, out string result)
        {
            var terms = line.SplitBySpace();

            for (int i = 0; i < terms.Count; i++)
            {
                if (_code.ContainsKey(terms[i]))
                {
                    result = null;
                    return false;
                }
                else if (terms[i].StartsWith("."))
                {
                    result = null;
                    return false;
                }
                
                if (!IsEnglish(terms[i]))
                {
                    terms.RemoveAt(i);
                    i--;
                    continue;
                }

                terms[i] = RemoveDigits(terms[i]);
            }

            result = string.Join(" ", terms.ToArray());

            if (!string.IsNullOrWhiteSpace(result))
                return true;
            else
                return false;
        }

        private string RemoveDigits(string input)
        {
            var result = new List<char>();
            foreach (var chr in input)
            {
                if (!_numbers.Contains(chr))
                    result.Add(chr);
            }
            return new string(result.ToArray());
        }

        private bool LineIsCode(string input)
        {
            foreach (var item in _code)
            {
                var key = item.Key;
                if (input.Contains(key))
                    return true;
            }
            if (input.TrimStart().StartsWith("."))
                return true;

            return false;
        }
        
        private bool IsEnglish(string input)
        {
            if (int.TryParse(input, out int res))
                return true;
            else if (float.TryParse(input, out float tf))
                return true;

            if (input.Length == 1)
            {
                if (_specialCharacters.ContainsKey(input[0]))
                    return false;
                else if (!Syntax.Articles.ContainsKey(input))
                    return false;
                else
                    return true;
            }

            var vowels = 0;

            foreach (var chr in input)
            {
                if (_vowels.ContainsKey(chr))
                    vowels++;
                else if (_specialCharacters.ContainsKey(chr))
                    return false;
            }

            if (vowels > 0)
                return true;
            else
                return false;
        }

        public static FriendlySorter New(string[] input)
        {
            return new FriendlySorter(input);
        }

        private Dictionary<char, int> _vowels = new Dictionary<char, int>()
        {
            {'a', 0 },
            {'A', 0 },
            {'i', 0 },
            {'I', 0 },
            {'o', 0 },
            {'O', 0 },
            {'e', 0 },
            {'E', 0 },
            {'u', 0 },
            {'U', 0 },
            {'y', 0 },
            {'Y', 0 },
        };

        private Dictionary<char, int> _specialCharacters = new Dictionary<char, int>()
        {
            {'{', 0 },
            {'}', 0 },
            {'\'', 0 },
            {'\"', 0 },
            {'&', 0 },
            {'*', 0 },
            {'^', 0 },
            {'$', 0 },
            {'#', 0 },
            {'@', 0 },
            {'=', 0 },
            {'_', 0 },
            {'\\', 0 },
            {'/', 0 },
            {'>', 0 },
            {'<', 0 },
        };

        private Dictionary<string, int> _containsButNotEqual = new Dictionary<string, int>()
        {
            {"page", 0 },
            {"family", 0 },
            {"title", 0 },
            {"link", 0 },
            {"domain", 0 },
            {"font", 0 },
        };

        private Dictionary<string, int> _code = new Dictionary<string, int>()
        {
            {"var", 0 },
            {"{", 0 },
            {"}", 0 },
            {"google", 0 },
            {".ads", 0 },
            {"function", 0 },
            {"span", 0 }
        };

        private List<char> _numbers = new List<char>()
        {
            '0',
            '1',
            '2',
            '3',
            '4',
            '5',
            '6',
            '7',
            '8',
            '9'
        };
    }
}
