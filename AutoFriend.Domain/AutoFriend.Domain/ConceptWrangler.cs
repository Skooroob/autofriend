﻿using AutoFriend.Domain.Grammar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using AutoFriend.Domain.Memory;
using AutoFriend.DTO.Grammar;
using AutoFriend.Persistence;
using AutoFriend.DTO.Memory;

namespace AutoFriend.Domain
{
    public class ConceptWrangler
    {
        private List<Sentence> Text { get; set; }
        private int _index;
        private IMemorable _memory;

        public ConceptWrangler(List<Sentence> sentences, IMemorable memory)
        {
            Text = sentences;
            _memory = memory;
        }

        public void Consume()
        {
            for (_index = 0; _index < Text.Count; _index++)
                LookForConcepts(Text[_index]);
            
            for (_index = 0; _index < Text.Count; _index++)
                RelateConcepts(Text[_index]);
        }



        private void LookForConcepts(Sentence sentence)
        {
            foreach (var term in sentence.Terms)
            {
                if (!IsGrammatical(term))
                {
                    if (!_memory.HasConcept(term))
                        _memory.AddConcept(new Concept(term));
                }
            }
        }

        private bool IsGrammatical(string term)
        {

            if (Syntax.Punctuation.ContainsKey(term))
                return true;
            else if (Syntax.Articles.ContainsKey(term))
                return true;
            else if (Syntax.Conjuctions.ContainsKey(term))
                return true;

            return false;
        }

        private void RelateConcepts(Sentence sentence)
        {
            foreach (var clause in sentence.Clauses)
            {
                foreach (var term in clause.Terms)
                {
                    if (IsGrammatical(term))
                        continue;

                    if (_memory.HasConcept(term))
                    {
                        _memory.AddClauseToConcept(term, clause);
                    }
                }
            }
        }

        public static ConceptWrangler New(List<Sentence> sentences, IMemorable memory)
        {
            return new ConceptWrangler(sentences, memory);
        }

    }
}
