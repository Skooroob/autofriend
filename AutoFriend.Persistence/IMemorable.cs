﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoFriend.DTO.Grammar;
using AutoFriend.DTO.Memory;

namespace AutoFriend.Persistence
{
    public interface IMemorable
    {
        bool HasConcept(string name);

        void AddConcept(Concept concept);

        void AddClauseToConcept(string name, Clause clause);

        Concept GetConcept(string name);
    }
}
