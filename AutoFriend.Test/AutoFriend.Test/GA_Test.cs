﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Troschuetz.Random;
using Troschuetz.Random.Generators;

using NUnit.Framework;
using AutoFriend.Domain;
using AutoFriend.Genetics;
using AutoFriend.Genetics.Concretions;
using AutoFriend.Genetics.Generation;
using AutoFriend.Genetics.DTO;
using Newtonsoft.Json;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;
using AutoFriend.Genetics.Persistence;

namespace AutoFriend.Test.Genetics
{
    [TestFixture]
    public class GA_Test
    {
        [Test]
        public void Make_Some_Nodes()
        {
            //Arrange
            var f = new Fun_With_Strings();
            var n = new NodeGenerator(f);

            //act
            var branch = n.GenerateNodes();
        }

        [Test]
        public void Link_Some_Nodes()
        {
            //Arrange
            var f = new Fun_With_Strings();
            var n = new NodeGenerator(f);

            //act
            var branches = n.GenerateTree(6);
        }

        [Test]
        public void Make_A_Branch()
        {
            //Arrange
            var f = new Fun_With_Strings();
            var n = new NodeGenerator(f);

            //act
            var branche = n.Tree();

        }
        
        [Test]
        public void Results_Should_Be_Same()
        {
            var fws = new FWS_Helper();
            var alf = new ALFGenerator(TMath.Seed());
            var thresh = alf.NextDouble();
            var weight = alf.NextDouble();

            var input = "two";

            var a = fws.Process(input, thresh, weight);
            var b = fws.Process(input, thresh, weight);

            Assert.AreEqual(a, b);
        }

        [Test]
        public void Results_Should_Be_Same_2()
        {
            var f = new Fun_With_Strings();
            var n = new NodeGenerator(new Fun_With_Strings());
            var branch = n.Tree();
            var input = "two";
            var root = branch.Nodes[0];

            var a = root.Call(input, branch);
            var b = root.Call(input, branch);

            Assert.AreEqual(a.Result, b.Result);
            Assert.AreEqual(a.NextNode.Id, b.NextNode.Id);
        }

        [Test]
        public void Run_Node_Program()
        {
            //Arrange
            var f = new Fun_With_Strings();
            var n = new NodeGenerator(f);
            var branch = n.Tree();

            var p = new BranchRunner(branch);

            var input = "two";
            var desiredOutput = "2";

            var output = p.Travel(input);

            Assert.AreNotEqual(input, output.Result);
            var nextOutPut = p.Travel(input);

            Assert.AreEqual(output.Result, nextOutPut.Result);

            while ((string)output.Result != desiredOutput)
            {
                var gen = new NodeGenerator(f);
                var br = gen.Tree();
                var runner = new BranchRunner(br);
                output = runner.Travel(input);
            }

            Assert.AreEqual(desiredOutput, output.Result);
        }

        [Test]
        public void Run_Node_Program_Two_DesiredOutputs()
        {
            //Arrange
            var n = new NodeGenerator(new Fun_With_Strings());
            var branch = n.Tree();

            var p = new BranchRunner(branch);

            var two = "two";
            var three = "three";

            var desiredTwo = "2";
            var desiredThree = "3";

            var output = p.Travel(two);
            var output2 = p.Travel(three);
            Assert.AreNotEqual(two, output.Result);
            var nextOutPut = p.Travel(two);

            Assert.AreEqual(output.Result, nextOutPut.Result);

            var a = false; var b = false;
            bool finished = false;
            while (!finished)
            {
                var gen = new NodeGenerator(new Fun_With_Strings());
                var br = gen.Tree();
                var runner = new BranchRunner(br);
                output = runner.Travel(two);
                output2 = runner.Travel(three);

                a = (string)output.Result == desiredTwo;
                b = (string)output2.Result == desiredThree;

                if (a || b)
                {
                    var foo = 0;
                }
                if (a && b)
                    finished = true;
            }

            Assert.AreEqual(desiredTwo, output.Result);
            Assert.AreEqual(desiredThree, output2.Result);

            //TODO: Use a chain of overlapping branches
        }

        [Test]
        public void Run_Node_Program_One_DesiredOutput_One_Tree()
        {
            //Arrange
            var o = new Outcome("two", "2");
            var list = new List<Outcome>() { o};
            var g = new GeneticAlgorithmGeneration(new Fun_With_Strings(), list);
            var algo = g.SolveForSet(0);

            var p = new BinaryPersistence();
            p.Save(algo, @"d:\data\two_to_2.algo");
        }

        [Test]
        public void Run_Node_Program_From_Disk()
        {
            //Arrange
            var p = new BinaryPersistence();
            var tree = p.LoadAsProgram(@"d:\data\two_to_2.algo", new Fun_With_Strings());

            var runner = new BranchRunner(tree);
            var output = runner.Travel("two");

            Assert.AreEqual("2", (string)output.Result);
        }

        [Test]
        public void Node_Program_Two_DesiredOutputs_One_Tree()
        {
            //Arrange
            var o = new Outcome("two", "2");
            var o2 = new Outcome("three", "3");
            var list = new List<Outcome>() { o, o2 };
            var g = new GeneticAlgorithmGeneration(new Fun_With_Strings(), list);
            var algo = g.SolveForSet(0);

            var p = new BinaryPersistence();
            p.Save(algo, @"d:\data\numbers_text.algo");
        }
    }
}
