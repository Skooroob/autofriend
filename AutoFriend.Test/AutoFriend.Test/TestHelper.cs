﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using AutoFriend.Domain;
using AutoFriend.DTO;
using AutoFriend.Persistence;

using AutoFriend.Domain.Memory;

using Moq;

namespace AutoFriend.Test
{
    public class TestHelper
    {
        public static string[] GetLines(string text)
        {
            return text.SplitByNewLine();
        }

        public static IMemorable MockPersistence()
        {
            return new MockMemory();
        }

        public static IUpdatable MockUpdate()
        {
            return new FakeUpdate();
        }


    }

    public class FakeUpdate : IUpdatable
    {
        public void Report(string message)
        {
        }
    }
}
