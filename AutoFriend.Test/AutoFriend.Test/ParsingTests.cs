﻿using System.Collections.Generic;

using NUnit.Framework;
using AutoFriend.Domain;
using AutoFriend.DTO;
using AutoFriend.Domain.Memory;
using AutoFriend.Domain.Grammar;


using StringCompare.Algorithms.Levenshtein;

using StringComparison;
using StringComparison.Enums;
using AutoFriend.Domain.Speech;

using Troschuetz.Random.Distributions.Continuous;
using Troschuetz.Random.Generators;
using Troschuetz.Random;
using System.Linq;
using System;

namespace AutoFriend.Test.Foundation
{
    [TestFixture]
    public class ParsingTests
    {
        [Test]
        public void ParserShouldDivideTextIntoSentences()
        {
            //Arrange
            var lines = TestHelper.GetLines(Data.Genesis_1.Ch_1_b);
            var reader = FriendlyGrammarParser.New(lines);

            //Act
            var sentences = reader.CreateSentences();
            var secondSentence = sentences[1];
            var firstClause = secondSentence.Clauses[0];

            //Assert
            Assert.AreEqual("And the earth was without form", firstClause.Text);

        }

        [Test]
        public void SorterShouldFixCamelCase()
        {
            //Arrange
            var lines = TestHelper.GetLines(Data.Genesis_1.Ch_1);
            var sorter = FriendlySorter.New(lines);
            var firstLine = lines[0];

            //Act
            var result = string.Empty;
            if (sorter.TryFixCamelCase(firstLine, out string foo))
            {
                result = foo;
            }  
            var terms = result.SplitBySpace();
            var sixth = terms[5];

            //Assert
            Assert.AreEqual("Edition", sixth);
        }
        
        [Test]
        public void WranglerShouldCreateConcepts()
        {
            //Arrange
            var lines = TestHelper.GetLines(Data.Genesis_1.Ch_1_b);
            var grammar = FriendlyGrammarParser.New(lines);
            var sentences = grammar.CreateSentences();

            //Act
            var memory = TestHelper.MockPersistence();
            var wrangler = ConceptWrangler.New(sentences, memory);
            wrangler.Consume();

            //Assert
            Assert.AreEqual(true, CurrentMemory.Concepts.ContainsKey("PURPLE"));
        }

        [Test]
        public void ConceptsShouldNotContainPunctuation()
        {
            //Arrange
            var lines = TestHelper.GetLines(Data.Genesis_1.Ch_1_b);
            var grammar = FriendlyGrammarParser.New(lines);
            var sentences = grammar.CreateSentences();

            //Act
            var memory = TestHelper.MockPersistence();
            var wrangler = ConceptWrangler.New(sentences, memory);
            wrangler.Consume();

            //Assert
            foreach (var pair in Syntax.Punctuation)
            {
                Assert.AreEqual(true, !CurrentMemory.Concepts.ContainsKey(pair.Key));
            }
        }

        [Test]
        public void ConceptsShouldBeRelated()
        {
            //Arrange
            var lines = TestHelper.GetLines(Data.Genesis_1.Ch_1_b);
            var grammar = FriendlyGrammarParser.New(lines);
            var sentences = grammar.CreateSentences();

            //Act
            var memory = TestHelper.MockPersistence();
            var wrangler = ConceptWrangler.New(sentences, memory);
            wrangler.Consume();
            var concept = CurrentMemory.Concepts["LIGHT"];

            //Assert
            Assert.AreEqual(true, concept.Relations.Contains("DARKNESS"));
        }

        [Test]
        public void StringCompareMeasureShouldDoSomething()
        {
            var A = "Hey hey ho ho merry xmas";
            var B = "Hee hee ha ha happy christmas";

            var lev = A.CompareLevenshtein(B);

            string source = A;
            string target = B;

            var options = new List<StringComparisonOption>();

            // Choose which algorithms should weigh in for the comparison
            options.Add(StringComparisonOption.UseOverlapCoefficient);
            options.Add(StringComparisonOption.UseLongestCommonSubsequence);
            options.Add(StringComparisonOption.UseLongestCommonSubstring);

            // Choose the relative strength of the comparison - is it almost exactly equal? or is it just close?
            var tolerance = StringComparisonTolerance.Strong;

            // Get a boolean determination of approximate equality
            bool result = source.IsSimilar(target, tolerance, options[0]);
            double howManySimilar = source.Similarity(target, options[0]);
            double simLevenshtein = source.LevenshteinDistancePercentage(target);
            double simJaro = source.JaroDistance(target);

            var foo = source.RatcliffObershelpSimilarity(target);

            var fee = source.SorensenDiceDistance(target);

            Assert.AreEqual(0.66037735849056611, fee);

            var fum = source.HammingDistance(target);

            var bar = source.JaccardDistance(target);

            var boo = source.JaroWinklerDistance(target);

        }
        
        [Test]
        public void StringsShouldNotBeSimilar()
        {
            var A = "And God said";
            var B = "And God blessed them";

            var result = A.SorensenDiceDistance(B);
            var foo = A.JaroWinklerDistance(B);

            Assert.AreEqual(true, result < 0.57f);
            
            var D = "and God said unto them";

            var result2 = A.SorensenDiceDistance(D);
            var bar = A.JaroWinklerDistance(D);

            Assert.AreEqual(true, result2 < 0.53f);

            var termsA = A.ToUpper().SplitBySpace();
            var termsD = D.ToUpper().SplitBySpace();

            var result3 = termsA.TermComparison(termsD);

            Assert.AreEqual(true, result3 > 0);

            var termsB = B.ToUpper().SplitBySpace();

            var result4 = termsA.TermComparison(termsB);

            Assert.AreEqual(true, result4 > 0);

            var avg = (result2 + result3) / 2.0f;//represents A, D
            var avg2 = (result + result4) / 2.0f;//represents A, B


            Assert.AreEqual(true, avg > avg2);

        }

        [Test]
        public void StringsShouldAlsoNotBeSimilar()
        {
            var A = "and let them have dominion over the fish of the sea";
            var B = "Let the earth bring forth the living creature after its kind";
            var C = "and the gathering together of the waters called He Seas";

            var at = A.SplitBySpace();
            var bt = B.SplitBySpace();
            var ct = C.SplitBySpace();


            var abAvg = (A.SorensenDiceDistance(B) + at.TermComparison(bt)) / 2.0f;
            var acAvg = (A.SorensenDiceDistance(C) + at.TermComparison(ct)) / 2.0f;

            Assert.AreEqual(true, abAvg < acAvg);

        }

        [Test]
        public void StringsShouldBeSimilar()
        {
            var A = "Let Us make man in Our image";
            var B = "So God created man in His Own image";
            var at = A.SplitBySpace();
            var bt = B.SplitBySpace();

            var sorenson = A.SorensenDiceDistance(B);
            var winkler = A.JaroWinklerDistance(B);
            var atDist = at.TermComparison(bt);

            var abAvg = (A.SorensenDiceDistance(B) + at.TermComparison(bt)) / 2.0f;

            Assert.AreEqual(true, sorenson > 0.56f);
        }

        [Test]
        public void SpeechShouldSaySomething()
        {
            //Arrange
            var lines = TestHelper.GetLines(Data.Genesis_1.Ch_1_b);
            var grammar = FriendlyGrammarParser.New(lines);
            var sentences = grammar.CreateSentences();

            //Act
            var memory = TestHelper.MockPersistence();
            var wrangler = ConceptWrangler.New(sentences, memory);
            wrangler.Consume();
            var speech = new BasicSpeech(memory);
            var result = speech.TalkAbout("earth");

            //Assert
            Assert.AreNotEqual(null, result);
        }

        [Test]
        public void SpeechShouldSaySomething2()
        {
            //Arrange
            var lines = TestHelper.GetLines(Data.Genesis_1.Ch_1_b);
            var grammar = FriendlyGrammarParser.New(lines);
            var sentences = grammar.CreateSentences();

            //Act
            var memory = TestHelper.MockPersistence();
            var wrangler = ConceptWrangler.New(sentences, memory);
            wrangler.Consume();
            var speech = new BasicSpeech(memory);
            var result = speech.TalkAbout("god");

            //Assert
            Assert.AreNotEqual(null, result);
        }

        [Test]
        public void ConceptsShouldHaveIntuitionsFromHtml()
        {
            //Arrange
            var parser = FriendlyHtmlParser.New(Data.Genesis_1.HTML, TestHelper.MockUpdate());
            var result = parser.ReadLines();
            var s = FriendlySorter.New(result);
            var lines = s.Sort();
            var gram = FriendlyGrammarParser.New(lines.ToArray());
            var sentences = gram.CreateSentences();
            
            //Act
            var memory = TestHelper.MockPersistence();
            var wrangler = ConceptWrangler.New(sentences, memory);
            wrangler.Consume();
            //var speech = new BasicSpeech(memory);
            //var result = speech.TalkAbout("god");

            var god = memory.GetConcept("god");

            ////Assert
            Assert.AreEqual(true, god.Intuitions.Count > 0);
        }

        [Test]
        public void NumberShouldBeRandom()
        {
            //Arrange
            var nr3 = new NR3Generator();
            var xor = new XorShift128Generator();
            var alf = new ALFGenerator(TMath.Seed());
            var rand = new TRandom();

            //Act
            var nrsingle = nr3.NextUInt(0, 100);
            var xorsingle = xor.NextUInt(0, 100);
            var alfsingle = alf.NextUInt(0, 100);
            var randsingle = rand.NextUInt(0, 100);


            var nrhundred = new int[100];
            for (int i = 0; i < nrhundred.Length; i++)
            {
                nrhundred[i] = (int)nr3.NextUInt(0, 100);
            }
            var nravg = nrhundred.ToList().Average();

            var xorhundred = new int[100];
            for (int i = 0; i < xorhundred.Length; i++)
            {
                xorhundred[i] = (int)xor.NextUInt(0, 100);
            }
            var xoravg = xorhundred.ToList().Average();

            var alfhundred = new int[100];
            for (int i = 0; i < alfhundred.Length; i++)
            {
                alfhundred[i] = (int)alf.NextUInt(0, 100);
            }
            var alfavg = alfhundred.ToList().Average();


            //Assert
            Assert.AreEqual(true, Math.Abs(50.0f - nravg) < 10);

        }

        //TODO: develop composition route
        //TODO: develop methods to rectify the same word, but plural, present progressive, etc.

    }
}
