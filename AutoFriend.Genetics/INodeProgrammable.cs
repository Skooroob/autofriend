﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AutoFriend.Genetics
{
    public interface INodeProgrammable
    {
        NodeOutput Process(object input, Node node, Tree branch);

        void Reset();

        INodeProgrammable New();
    }
}
