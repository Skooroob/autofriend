﻿using AutoFriend.Genetics.Generation;
using System;

using System.Collections.Generic;
using Troschuetz.Random;
using Troschuetz.Random.Generators;

namespace AutoFriend.Genetics
{
    public class NodeGenerator
    {

        private INodeProgrammable _program;
        private const int _maxLinkTries = 10000000;

        public NodeGenerator(INodeProgrammable program)
        {
            _program = program;
            
            //Vine.Index = 0;
        }

        public Tree GenerateNodes()
        {
            var vine = new Vine();
            vine.Sprouts = 0;
            vine.MaxGenerations = 4;
            var root = new Node(_program, vine);
            root.Grow(0);
            var linker = new TreeLinker();
            var tree = new Tree(vine.Links, vine.AllNodes);
            tree = linker.Mirror(tree);
            tree = linker.ThickenConnections(tree);

            return new Tree(vine.Links, vine.AllNodes);
        }

        public List<Tree> GenerateTree(int branches)
        {
            var result = new List<Tree>();
            for (int i = 0; i < branches; i++)
                result.Add(GenerateNodes());
            
            return result;
        }

        public Tree Join(List<Tree> branches)
        {

            var nodes = new Dictionary<int, Node>();
            var links = new Dictionary<Link, ChildWeight>();
            foreach (var branch in branches)
            {
                nodes = StripNode(branch, nodes);
                links = StripLink(branch, links);
            }
            var nodeCount = nodes.Count - 1;

            foreach (var k in nodes)
            {
                var node = k.Value;
                var connections = 0;
                var alf = new ALFGenerator(TMath.Seed());
                var num = alf.NextUInt(8, 16);
                var linkTries = 0;
                while (connections < num)
                {
                    if (linkTries > _maxLinkTries)
                        break;

                    linkTries++;
                    var id = (int)alf.NextUInt(0, (uint)nodeCount);

                    if (id == node.Id || id < node.Id)
                        continue;

                    var reach = (int)((double)nodeCount * alf.NextDouble(0.025, .1));
                    var distance = Math.Abs(node.Id - id);
                    if (distance > reach)
                        continue;


                    var link = new Link(node.Id, id, alf.NextDouble());
                    if (!links.ContainsKey(link))
                    {
                        connections++;
                        links.Add(link, new ChildWeight(nodes[id], alf.NextDouble()));
                        node.Children.Add(nodes[id]);
                    }
                }
            }
            return new Tree(links, nodes);
        }

        public Tree Tree()
        {
            //var alf = new ALFGenerator(TMath.Seed());
            //var no_of_branches = 1;
            //var branches = GenerateTree(no_of_branches);
            //return Join(branches);
            return GenerateNodes();
        }
        
        private Dictionary<int, Node> StripNode(Tree branch, Dictionary<int, Node> master)
        {
            foreach (var node in branch.Nodes)
                master.Add(node.Key, node.Value);
            return master;
        }

        private Dictionary<Link, ChildWeight> StripLink(Tree branch, Dictionary<Link, ChildWeight> master)
        {
            foreach (var link in branch.Links)
                master.Add(link.Key, link.Value);
            return master;
        }
    }
}
