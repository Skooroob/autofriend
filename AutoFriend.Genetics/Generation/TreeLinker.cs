﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Troschuetz.Random;
using Troschuetz.Random.Generators;

namespace AutoFriend.Genetics.Generation
{
    public class TreeLinker
    {
        private int _maxGenerationalDepth = 4;
        private Tree _tree;

        public Tree Combine(Tree A, Tree B)
        {

            //Parent B to A, essentially
            var A_Nodes = A.Nodes.Values.ToArray();



            return null;
        }

        public Tree Mirror(Tree tree)
        {
            var low = tree.Links.Values.Where(k => k.Child.Children.Count == 0).ToList();
            var lowestLayer = new List<Node>();
            foreach (var k in low)
                lowestLayer.Add(k.Child);
            
            CreateLowerGenerations:
            var first = lowestLayer.First();
            var vine = first._Vine;
            var index = 0;
            var layer = new List<Node>();
            var nuchild = new Node(first._program,vine);
            nuchild.Generation = first.Generation + 1;
            layer.Add(nuchild);
            var factor = (int)((double)lowestLayer.Count * 0.05f);
            if (factor == 1)
                factor = lowestLayer.Count;
            
            foreach (var node in lowestLayer)
            {
                if (index != 0 && index % factor == 0)
                {
                    nuchild = new Node(first._program, vine);
                    nuchild.Generation = first.Generation + 1;
                    layer.Add(nuchild);
                }
                node.Parent(nuchild);
                index++;
            }

            lowestLayer = layer;
            if (lowestLayer.Count == 1)
                return tree;
            else
                goto CreateLowerGenerations;
        }

        public Tree ThickenConnections(Tree tree)
        {
            _tree = tree;
            var root = tree.Nodes.Values.First();
            LinkDown(root);
            return tree;
        }

        private void LinkDown(Node node)
        {
            if (node.Children.Count == 0)
                return;

            node.Children.ForEach(k => LinkDown(k));

            if (node.Generation == 0)
                return;
            
            var alf = new ALFGenerator(TMath.Seed());
            var connections = alf.NextUInt(2, 7);
            for (int i = 0; i < connections; i++)
            {
                var kid = TryGetKid(node, 15);
                if (kid == null)
                    break;

                if (node.Children.FirstOrDefault(k => k.Id == kid.Id) == null)
                    node.Parent(kid);
            }
        }

        private Node TryGetKid(Node node, int maxTries)
        {
            var kid = GetRandomChild(node);
            var tries = 0;
            while (kid == null && tries < maxTries)
            {
                kid = GetRandomChild(node);
                if (kid != null)
                    return kid;
                tries++;
            }
            return null;
        }

        private Node GetRandomChild(Node parent)
        {
            if (parent.Children.Count == 0)
                return null;

            var alf = new ALFGenerator(TMath.Seed());

            if (parent.Generation != 0)
            {
                var brethren = _tree.Nodes.Values
                    .Where(k => k.Generation == parent.Generation).ToList();
                var brother = (int)alf.NextUInt((uint)brethren.Count);
                parent = brethren[brother];
            }
            
            var generations = alf.NextUInt(2, (uint)_maxGenerationalDepth);
            var nextNode = parent;
            for (int i = 0; i < generations; i++)
            {
                if (nextNode.Children.Count == 0)
                    return null;

                var index = (int)alf.NextUInt((uint)nextNode.Children.Count);
                nextNode = nextNode.Children[index];

                if (CyclicalRelationship(parent, nextNode))
                    return null;
            }

            return nextNode;
        }

        private bool CyclicalRelationship(Node parent, Node child)
        {
            foreach (var k in child.Children)
            {
                if (k.Id == parent.Id)
                    return true;
                else if (CyclicalRelationship(parent, k))
                    return true;
            }

            return false;
        }
    }
}
