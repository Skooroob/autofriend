﻿using System;
using System.Collections.Generic;
using Troschuetz.Random;
using Troschuetz.Random.Generators;

namespace AutoFriend.Genetics
{
    public class Node
    {
        public int Id { get; set; }
        public int Generation { get; set; }
        public double Threshold { get; set; }
        public INodeProgrammable _program;
        public List<Node> Children { get; set; }
        public Vine _Vine { get; set; }

        public Node(INodeProgrammable program, Vine vine)
        {
            _Vine = vine;
            _program = program;
            Init();
        }

        public Node()
        {
            Children = new List<Node>();
        }

        private void Init()
        {
            Id = _Vine.Index;
            _Vine.Index++;
            _Vine.Sprouts++;
            _Vine.AllNodes.Add(Id, this);
            var alf = new ALFGenerator(TMath.Seed());
            Threshold = alf.NextDouble();
            Children = new List<Node>();
        }
        
        public void Grow(int generation)
        {
            Generation = generation;
            if (generation > _Vine.MaxGenerations)
                return;

            var alf = new ALFGenerator(TMath.Seed());
            var times = alf.NextUInt(2, 6);
            for (int i = 0; i < times; i++)
            {
                var child = new Node(_program, _Vine);
                LinkToNode(child);
                Children.Add(child);
            }
            
            foreach (var child in Children)
                child.Grow(generation + 1);
        }

        public void LinkToNode(Node child)
        {
            var alf = new ALFGenerator(TMath.Seed());
            var link = new Link(Id, child.Id, alf.NextDouble());
            _Vine.Links.Add(link, new ChildWeight(child, link.Weight));
        }

        public void Parent(Node child)
        {
            var alf = new ALFGenerator(TMath.Seed());
            var link = new Link(Id, child.Id, alf.NextDouble());
            _Vine.Links.Add(link, new ChildWeight(child, link.Weight));
            Children.Add(child);
        }

        public NodeOutput Call(object input, Tree branch)
        {
            var programResult = _program.Process(input, this, branch);
            return programResult;
        }

        public override string ToString()
        {
            return Id.ToString();
        }

    }
}
