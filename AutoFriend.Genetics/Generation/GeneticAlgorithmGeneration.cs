﻿using AutoFriend.Genetics.DTO;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AutoFriend.Genetics.Generation
{
    public class GeneticAlgorithmGeneration
    {
        public INodeProgrammable Program { get; set; }
        public List<Outcome> IO_Set { get; set; }
        public bool Success { get; set; }
        public Tree Result { get; set; }
        private int _tries;
        public GeneticAlgorithmGeneration(INodeProgrammable program, List<Outcome> io_set)
        {
            Program = program;
            IO_Set = io_set;
        }


        //basically, at the end, we save the Tree (or set of trees) and use it again. 
        public Tree SolveForSet(int conditionForSuccess, int maxTries = int.MaxValue)
        {



            ///var l = new List<int> { 12, 9, 8, 7, 6, 5, 4 };
            //Parallel.ForEach(l, n =>
            //{
            //    SolveThread(conditionForSuccess, maxTries);
            //});

            for (int i = 0; i < 12; i++)
            {
                var t = new Thread(() =>
                {
                    SolveThread(conditionForSuccess, maxTries);
                });
                t.Start();
            }

            SolveThread(conditionForSuccess, maxTries);

            return Result;
        }

        public void SolveThread(int conditionForSuccess, int maxTries = int.MaxValue)
        {
            if (Success)
                return;

            if (conditionForSuccess <= 0)
                conditionForSuccess = IO_Set.Count;
            
            bool finished = false;
            while (!finished)
            {
                if (_tries > maxTries)
                    break;
                else if (Success)
                    break;

                var wins = 0;
                var gen = new NodeGenerator(Program.New());
                var tree = gen.Tree();
                var runner = new BranchRunner(tree);

                var outputs = new List<string>();
                foreach (var pair in IO_Set)
                {
                    var output = (string)runner.Travel(pair.Input).Result;
                    outputs.Add(output);
                    if (output == pair.Output.ToString())
                        wins++;
                }

                if (wins >= conditionForSuccess)
                {
                    Result = tree;
                    Success = true;
                    break;
                }

                _tries++;
            }
        }

    }
}
