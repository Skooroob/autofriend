﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AutoFriend.Genetics.DTO
{
    public class Outcome
    {
        public object Input { get; set; }
        public object Output { get; set; }
        public Outcome(object input, object output)
        {
            Input = input;
            Output = output;
        }
    }
}
