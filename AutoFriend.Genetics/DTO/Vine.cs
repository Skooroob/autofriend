﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AutoFriend.Genetics
{
    public class Vine
    {
        public int Index;
        public Dictionary<Link, ChildWeight> Links;
        public Dictionary<int, Node> AllNodes;
        public int Sprouts;
        public int MaxGenerations;

        public Vine()
        {
            Links = new Dictionary<Link, ChildWeight>();
            AllNodes = new Dictionary<int, Node>();
        }

        public Vine(int maxGenerations)
        {
            MaxGenerations = maxGenerations;
            Links = new Dictionary<Link, ChildWeight>();
            AllNodes = new Dictionary<int, Node>();
        }
    }
}
