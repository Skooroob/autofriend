﻿using System;

namespace AutoFriend.Genetics
{
    [Serializable]
    public struct Link
    {
        public int Parent;
        public int Child;
        public double Weight;
        
        public Link(int parent, int child, double weight)
        {
            Parent = parent;
            Child = child;
            Weight = weight;
        }

        public Link(int parent, int child)
        {
            Parent = parent;
            Child = child;
            Weight = 0;
        }

        public override bool Equals(object obj) =>
            obj is Link && this == (Link)obj;

        public override int GetHashCode() =>
            Parent.GetHashCode() ^ Child.GetHashCode();

        public static bool operator ==(Link x, Link y) => 
            (x.Parent == y.Parent && x.Child == y.Child) ||
            (x.Child == y.Parent && x.Parent == y.Child);

        public static bool operator !=(Link x, Link y) =>
            !(x == y);
    }
}
