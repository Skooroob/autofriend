﻿namespace AutoFriend.Genetics
{
    public class NodeOutput
    {
        public object Result { get; set; }
        public Node NextNode { get; set; }
        public NodeOutput(object result, Node nextNode)
        {
            Result = result;
            NextNode = nextNode;
        }
    }
}
