﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AutoFriend.Genetics
{
    public class ChildWeight
    {
        public Node Child { get; set; }
        public double Weight { get; set; }
        public ChildWeight(Node child, double weight)
        {
            Child = child;
            Weight = weight;
        }
    }
}
