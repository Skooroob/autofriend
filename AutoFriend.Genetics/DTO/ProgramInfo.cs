﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AutoFriend.Genetics
{
    public class ProgramInfo
    {
        public int NodesTravelled { get; set; }
        public List<double> Thresholds { get; set; }
        public List<double> Weights { get; set; }
    }
}
