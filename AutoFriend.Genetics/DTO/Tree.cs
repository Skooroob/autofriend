﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AutoFriend.Genetics
{

    public class Tree
    {
        public Dictionary<Link, ChildWeight> Links;
        public Dictionary<int, Node> Nodes;
        public Tree(Dictionary<Link, ChildWeight> links, Dictionary<int, Node> allNodes)
        {
            Links = links;
            Nodes = allNodes;
        }
        public Tree() { }
    }
}
