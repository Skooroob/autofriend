﻿namespace AutoFriend.Genetics
{
    public class BranchRunner
    {
        private Tree _branch;

        public BranchRunner(Tree branch)
        {
            _branch = branch;
        }

        public NodeOutput Travel(object input)
        {
            var root = _branch.Nodes[0];
            root._program.Reset();
            var output = root.Call(input, _branch);
            var node = output.NextNode;
            bool run = true;
            int steps = 0;
            while (run)
            {
                root = node;
                var innerOutput = root.Call(output.Result, _branch);
                if (innerOutput == null)
                    break;
                output = innerOutput;
                if (output.NextNode == null)
                    break;
                node = output.NextNode;
                steps++;
            }

            return output;
        }
    }
}
