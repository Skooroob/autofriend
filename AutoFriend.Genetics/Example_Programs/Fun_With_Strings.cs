﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;

namespace AutoFriend.Genetics.Concretions
{
    public class Fun_With_Strings : INodeProgrammable
    {
        private List<double> _thresholds;
        private List<double> _weights;
        public Fun_With_Strings()
        {
            Init();
        }
       
        public NodeOutput Process(object input, Node node, Tree branch)
        {
            var s = (string)input;
            var threshold = node.Threshold;

            if (node.Children.Count == 0)
            {
                return new NodeOutput(Terminate(s), null);
            }
            
            var cw = GetChildWeight(threshold, InputThreshold(s, threshold), branch, node);
            var weight = cw.Weight;
            var helper = new FWS_Helper();
            var str = helper.Process(s, threshold, weight);
            var result = new NodeOutput(str, cw.Child);
            _thresholds.Add(threshold);
            _weights.Add(weight);
            return result;
        }

        private string Terminate(string input)
        {
            var chrs = new List<char>();
            foreach (var ch in input)
                if (char.IsDigit(ch))
                    chrs.Add(ch);
            if (chrs.Count == 0)
                return input;
            else
                return new string(chrs.ToArray());
        }

        private ChildWeight GetChildWeight(double threshold, double inThresh, Tree branch, Node node)
        {
            var linking = new ConcurrentBag<Link>();

            foreach (var child in node.Children)
                linking.Add(new Link(node.Id, child.Id));

            if (linking.Count == 0)
                return new ChildWeight(null, 0);

            var mult = (threshold + inThresh)/2.0;
            //var mult = inThresh;
            
            var target = mult * (double)linking.Count;
            var index = 0;
            double lastDistance = double.MaxValue;
            var ls = linking.ToList();
            for (int i = 0; i < ls.Count; i++)
            {
                var distance = Math.Abs(target - (double)i);
                if (distance < lastDistance)
                {
                    lastDistance = distance;
                    index = i;
                }
                else
                    break;
            }

            var chosenLink = ls[index];
            var that = branch.Links[chosenLink];
            return that;
        }

        public static int[] Unwanted;

        public void Init()
        {
            var bads = new List<int>();
            for (int i = 0; i < 48; i++)
            {
                bads.Add(i);
            }

            for (int i = 58; i < 65; i++)
            {
                bads.Add(i);
            }

            for (int i = 91; i < 97; i++)
            {
                bads.Add(i);
            }

            for (int i = 123; i < 128; i++)
            {
                bads.Add(i);
            }
            Unwanted = bads.ToArray();
            Reset();
        }

        public void Reset()
        {
            _thresholds = new List<double>();
            _weights = new List<double>();
        }

        public INodeProgrammable New()
        {
            return new Fun_With_Strings();
        }


        public double InputThreshold(string input, double threshold)
        {
            var helperWeight = HelperWeight(threshold);

            var vals = new List<int>();
            var even = true;
            if (threshold > 0.5)
                even = false;


            foreach (var ch in input)
            {
                var bte = Encoding.ASCII.GetBytes(ch.ToString());
                var val = (int)bte[0];
                if (even)
                {
                    val = val << 1;
                    even = false;
                }
                else
                {
                    val = val >> 1;
                    even = true;
                }
                vals.Add(val);
            }

            var avg = vals.Average();
            var max = (double)vals.Max();

            var min = (double)vals.Min();
            var avg2 = (min + max) / 2.0;
            
            var inputBasedVariable = ((avg-min) / max) + helperWeight;
            if (inputBasedVariable > 1)
                inputBasedVariable = 1;

            //we are dealing with an ascii byte[]
            //where
            // 64 32  16  8  4  2  1  0
            //int foo = 1;
            //for (int i = 0; i < 8; i++)
            //{
            //    foo *= 2;
            //}

            //for (int i = 5; i >1; i--)
            //{
            //    foo = foo >> 1;
            //}

            //i << 1
            
            if (avg > avg2)
            {
                if (inputBasedVariable > 0.5)
                {
                    //if (inputBasedVariable > helperWeight)
                    //    inputBasedVariable = inputBasedVariable - helperWeight;
                    //else
                    //    inputBasedVariable = helperWeight - inputBasedVariable;
                    

                }
                else if (inputBasedVariable < 0.5)
                {
                    //if (inputBasedVariable + helperWeight < 1)
                    //    inputBasedVariable = inputBasedVariable + helperWeight;
                }
            }
            else if (inputBasedVariable > threshold)
                inputBasedVariable = inputBasedVariable - threshold;

            return inputBasedVariable;
        }

        private double HelperWeight(double threshold)
        {
            double helperWeight = 0;
            if (_weights.Count == 0)
                return helperWeight;

            var index = (int)(threshold * _weights.Count);

            return _weights[_weights.Count-1];
        }

        public static byte[] ObjectToByteArray(Object obj)
        {
            BinaryFormatter bf = new BinaryFormatter();
            using (var ms = new MemoryStream())
            {
                bf.Serialize(ms, obj);
                return ms.ToArray();
            }
        }

        public static Object ByteArrayToObject(byte[] arrBytes)
        {
            using (var memStream = new MemoryStream())
            {
                var binForm = new BinaryFormatter();
                memStream.Write(arrBytes, 0, arrBytes.Length);
                memStream.Seek(0, SeekOrigin.Begin);
                var obj = binForm.Deserialize(memStream);
                return obj;
            }
        }
    }
}
