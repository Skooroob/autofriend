﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AutoFriend.Genetics.Concretions
{
    public class FWS_Helper
    {

        public string Process(string input, double threshold, double weight)
        {
            var s = (string)input;
            var bytes = Encoding.ASCII.GetBytes(s);
            var len = bytes.Length;

            if (threshold > 0.5 || weight > 0.5)
                s = s.ToCharArray().Reverse().ToString();

            var thresh = (int)((double)len * threshold);

            var wgt = (int)((double)len * weight);
            double avg = (threshold + weight) / 2.0f;
            int diff = (int)Math.Abs(threshold * 2.0f - weight * 2.0f);
            if (diff >= len)
                diff = 0;

            if (threshold < weight)
                diff *= -1;

            var nulen = len + diff;
            
            var big = threshold + weight;
            if (big > 1)
                big = 1;

            var small = threshold - weight;
            if (small < 0)
                small = 0;

            var threshKey = (int)((double)123 * threshold);
            var weightKey = (int)((double)123 * weight);
            var avgKey = (int)((double)123 * avg);
            var xavgKey = (int)((double)123 * ((threshold + avg) / 2));
            
            var over = 0;
            var nuBytes = new List<Byte>();
            for (int i = 0; i < nulen; i++)
            {
                if (i > len - 1)
                {
                    if (over == 0 && !Unwanted(avgKey))
                    {
                        Byte myByte = (byte)avgKey;
                        nuBytes.Add(myByte);
                    }
                    else if(!Unwanted(xavgKey))
                    {
                        Byte myByte = (byte)xavgKey;
                        nuBytes.Add(myByte);
                    }
                    over++;
                }
                else
                {
                    if (i == thresh && !Unwanted(weightKey))
                    {
                        Byte myByte = (byte)weightKey;
                        nuBytes.Add(myByte);
                    }
                    else if (i == wgt && !Unwanted(threshKey))
                    {
                        Byte myByte = (byte)threshKey;
                        nuBytes.Add(myByte);
                    }
                    else
                    {
                        nuBytes.Add(bytes[i]);
                    }
                }
            }

            var arr = nuBytes.ToArray();
            var str = Encoding.ASCII.GetString(arr);
            return str;
        }

        public bool Unwanted(int ch)
        {
            return Fun_With_Strings.Unwanted.Contains(ch);
        }
    }
}
