﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AutoFriend.Genetics.Node_Programs
{
    public class FWS_Info : ProgramInfo
    {
        public int MyProperty { get; set; }

        public FWS_Info()
        {
            Weights = new List<double>();
            Thresholds = new List<double>();
        }
    }
}
