﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

namespace AutoFriend.Genetics.Persistence
{
    public class BinaryPersistence : ITreeSaver
    {
        public Tree LoadAsProgram(object address, INodeProgrammable program)
        {
            SerializableTree ser = null;
            FileStream fs = new FileStream((string)address, FileMode.Open);
            try
            {
                BinaryFormatter formatter = new BinaryFormatter();
                ser = (SerializableTree)formatter.Deserialize(fs);
            }
            catch (SerializationException e)
            {
                Console.WriteLine("Failed to deserialize. Reason: " + e.Message);
                throw;
            }
            finally
            {
                fs.Close();
            }

            var link_dictionary = new Dictionary<Link, ChildWeight>();
            var node_dictionary = new Dictionary<int, Node>();
            
            var s_links = ser.Links;
            var s_nodes = ser.Nodes;

            var nodes = new List<Node>();

            foreach (var nd in s_nodes)
            {
                var node = new Node();
                node.Id = nd.Id;
                node.Generation = nd.Generation;
                node.Threshold = nd.Threshold;
                node._program = program;
                nodes.Add(node);
                node_dictionary.Add(node.Id, node);
            }

            foreach (var node in nodes)
            {
                var child_Links = s_links.Where(k => k.Parent == node.Id).ToList();
                var kids = new List<Node>();
                foreach (var link in child_Links)
                {
                    var kid = nodes.First(k => k.Id == link.Child);
                    var cw = new ChildWeight(kid, link.Weight);
                    node.Children.Add(kid);
                    link_dictionary.Add(link, cw);
                }
            }

            return new Tree(link_dictionary, node_dictionary);
        }
        

        public void Save(Tree tree, object address)
        {
            FileStream fs = new FileStream((string)address, FileMode.Create);

            BinaryFormatter formatter = new BinaryFormatter();
            try
            {
                var t = new SerializableTree(tree);
                formatter.Serialize(fs, t);
            }
            catch (SerializationException e)
            {
                Console.WriteLine("Failed to serialize. Reason: " + e.Message);
                throw;
            }
            finally
            {
                fs.Close();
            }
        }
    }
}
