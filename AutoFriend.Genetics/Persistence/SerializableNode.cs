﻿using System;

namespace AutoFriend.Genetics.Persistence
{
    [Serializable]
    public class SerializableNode
    {
        public int Id;
        public int Generation;
        public double Threshold;

        public SerializableNode(Node node)
        {
            Id = node.Id;
            Generation = node.Generation;
            Threshold = node.Threshold;
        }
    }
}
