﻿namespace AutoFriend.Genetics.Persistence
{
    public interface ITreeSaver
    {
        void Save(Tree tree, object address);

        Tree LoadAsProgram(object address, INodeProgrammable program);
    }
}
