﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AutoFriend.Genetics.Persistence
{
    [Serializable]
    public class SerializableTree
    {
        public SerializableNode[] Nodes;
        public Link[] Links;

        public SerializableTree(Tree tree)
        {
            Links = tree.Links.Keys.ToArray();
            Nodes = GetNodes(tree);
        }

        private SerializableNode[] GetNodes(Tree tree)
        {
            var nl = tree.Nodes.Values.ToList();

            var nodes = new List<SerializableNode>();
            foreach (var n in nl)
            {
                var node = new SerializableNode(n);
                nodes.Add(node);
            }
            
            return nodes.ToArray();
        }

    }
}
