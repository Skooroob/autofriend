﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutoFriend.DTO
{
    public static class StringExtensions
    {
        public static string[] SplitByNewLine(this string input)
        {
            return input.Split(new string[] {Environment.NewLine, "\n" }, StringSplitOptions.RemoveEmptyEntries);
        }

        public static List<string> SplitBySpace(this string input)
        {
            return input.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries).ToList();
        }

        public static double TermComparison(this List<string> A, List<string> B)
        {
            float medianCount = 0;
            if (B.Count > A.Count)
                medianCount = B.Count;
            else if (B.Count < A.Count)
                medianCount = A.Count;
            else
                medianCount = A.Count;


            var AMatches = new List<int>();
            var BMatches = new List<int>();
            var matches = 0;

            for (int i = 0; i < A.Count; i++)
            {
                if (AMatches.Contains(i))
                    continue;

                for (int k = 0; k < B.Count; k++)
                {
                    if (BMatches.Contains(k))
                        continue;

                    if (A[i] == B[k])
                    {
                        AMatches.Add(i);
                        BMatches.Add(k);
                        matches++;
                        break;
                    }
                }
            }

            for (int i = 0; i < B.Count; i++)
            {
                if (BMatches.Contains(i))
                    continue;

                for (int k = 0; k < A.Count; k++)
                {
                    if (AMatches.Contains(k))
                        continue;

                    if (B[i] == A[k])
                    {
                        AMatches.Add(k);
                        BMatches.Add(i);
                        matches++;
                    }
                }
            }
            
            return (float)matches / medianCount;
        }

        public static double TermComparison(this string[] A, string[] B)
        {
            float medianCount = 0;
            if (B.Length > A.Length)
                medianCount = B.Length;
            else if (B.Length < A.Length)
                medianCount = A.Length;
            else
                medianCount = A.Length;


            var AMatches = new List<int>();
            var BMatches = new List<int>();
            var matches = 0;

            for (int i = 0; i < A.Length; i++)
            {
                if (AMatches.Contains(i))
                    continue;

                for (int k = 0; k < B.Length; k++)
                {
                    if (BMatches.Contains(k))
                        continue;

                    if (A[i] == B[k])
                    {
                        AMatches.Add(i);
                        BMatches.Add(k);
                        matches++;
                        break;
                    }
                }
            }

            for (int i = 0; i < B.Length; i++)
            {
                if (BMatches.Contains(i))
                    continue;

                for (int k = 0; k < A.Length; k++)
                {
                    if (AMatches.Contains(k))
                        continue;

                    if (B[i] == A[k])
                    {
                        AMatches.Add(k);
                        BMatches.Add(i);
                        matches++;
                    }
                }
            }

            return (float)matches / medianCount;
        }
    }
}
