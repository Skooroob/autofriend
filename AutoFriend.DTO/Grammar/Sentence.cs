﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutoFriend.DTO.Grammar
{
    public class Sentence
    {
        public string Text { get; set; }
        public string[] Terms { get; set; }
        public Clause[] Clauses { get; set; }
        public bool IsQuestion { get; set; }

        public Sentence(List<string> words)
        {
            var s = string.Join(" ", words);
            Text = s;
            GetTerms();
        }

        public Sentence(List<string> words, bool question)
        {
            var s = string.Join(" ", words);
            Text = s;
            IsQuestion = question;
            GetTerms();
        }

        private void GetTerms()
        {
            if (Text == null)
                return;

            Terms = Text.SplitBySpace().ToArray();
        }

        public override string ToString()
        {
            return Text;
        }
    }
}
