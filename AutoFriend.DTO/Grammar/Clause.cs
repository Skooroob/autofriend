﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutoFriend.DTO.Grammar
{
    public class Clause
    {
        public string Text { get; set; }
        public string[] Terms { get; set; }

        public Clause(string text)
        {
            Text = text;
            GetTerms();
        }

        private void GetTerms()
        {
            if (Text == null)
                return;

            Terms = Text.ToUpper().SplitBySpace().ToArray();
        }

        public override string ToString()
        {
            return Text;
        }
    }
}
