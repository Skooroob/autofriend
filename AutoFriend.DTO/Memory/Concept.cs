﻿using AutoFriend.DTO.Grammar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutoFriend.DTO.Memory
{
    public class Concept
    {
        public string Name { get; set; }

        public List<Clause> Intuitions { get; set; }

        public List<string> Relations { get; set; }

        public Concept(string name)
        {
            Name = name.ToUpper();
            Intuitions = new List<Clause>();
            Relations = new List<string>();
        }
    }
}
