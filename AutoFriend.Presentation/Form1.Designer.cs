﻿namespace AutoFriend
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.AutoFriendOutputTextBox = new System.Windows.Forms.TextBox();
            this.UrlInputTextBox = new System.Windows.Forms.TextBox();
            this.GoButton = new System.Windows.Forms.Button();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.SubjectInputTextBox = new System.Windows.Forms.TextBox();
            this.TalkAboutButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // AutoFriendOutputTextBox
            // 
            this.AutoFriendOutputTextBox.Location = new System.Drawing.Point(12, 12);
            this.AutoFriendOutputTextBox.Multiline = true;
            this.AutoFriendOutputTextBox.Name = "AutoFriendOutputTextBox";
            this.AutoFriendOutputTextBox.ReadOnly = true;
            this.AutoFriendOutputTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.AutoFriendOutputTextBox.Size = new System.Drawing.Size(475, 225);
            this.AutoFriendOutputTextBox.TabIndex = 0;
            // 
            // UrlInputTextBox
            // 
            this.UrlInputTextBox.AllowDrop = true;
            this.UrlInputTextBox.Location = new System.Drawing.Point(12, 243);
            this.UrlInputTextBox.Name = "UrlInputTextBox";
            this.UrlInputTextBox.Size = new System.Drawing.Size(439, 20);
            this.UrlInputTextBox.TabIndex = 1;
            // 
            // GoButton
            // 
            this.GoButton.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.GoButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.GoButton.Location = new System.Drawing.Point(457, 240);
            this.GoButton.Name = "GoButton";
            this.GoButton.Size = new System.Drawing.Size(29, 23);
            this.GoButton.TabIndex = 2;
            this.GoButton.Text = "Go";
            this.GoButton.UseVisualStyleBackColor = false;
            this.GoButton.Click += new System.EventHandler(this.GoButton_Click);
            // 
            // SubjectInputTextBox
            // 
            this.SubjectInputTextBox.Location = new System.Drawing.Point(13, 270);
            this.SubjectInputTextBox.Name = "SubjectInputTextBox";
            this.SubjectInputTextBox.Size = new System.Drawing.Size(100, 20);
            this.SubjectInputTextBox.TabIndex = 3;
            // 
            // TalkAboutButton
            // 
            this.TalkAboutButton.Location = new System.Drawing.Point(120, 266);
            this.TalkAboutButton.Name = "TalkAboutButton";
            this.TalkAboutButton.Size = new System.Drawing.Size(75, 23);
            this.TalkAboutButton.TabIndex = 4;
            this.TalkAboutButton.Text = "Talk About";
            this.TalkAboutButton.UseVisualStyleBackColor = true;
            this.TalkAboutButton.Click += new System.EventHandler(this.TalkAboutButton_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(499, 545);
            this.Controls.Add(this.TalkAboutButton);
            this.Controls.Add(this.SubjectInputTextBox);
            this.Controls.Add(this.GoButton);
            this.Controls.Add(this.UrlInputTextBox);
            this.Controls.Add(this.AutoFriendOutputTextBox);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox AutoFriendOutputTextBox;
        private System.Windows.Forms.TextBox UrlInputTextBox;
        private System.Windows.Forms.Button GoButton;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.TextBox SubjectInputTextBox;
        private System.Windows.Forms.Button TalkAboutButton;
    }
}

