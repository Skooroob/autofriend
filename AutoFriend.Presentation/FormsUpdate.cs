﻿using AutoFriend.Domain;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutoFriend
{
    public class FormsUpdate : IUpdatable
    {
        private BackgroundWorker _worker;
        public FormsUpdate(BackgroundWorker worker)
        {
            _worker = worker;
            _worker.WorkerReportsProgress = true;
        }

        public void Report(string report)
        {
            _worker.ReportProgress(1, report);
        }
    }
}
