﻿using AutoFriend.Domain;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using System.IO;
using AutoFriend.Domain.Memory;
using AutoFriend.Domain.Speech;

namespace AutoFriend
{
    public partial class Form1 : Form
    {

        private string output = string.Empty;
        public Form1()
        {
            InitializeComponent();
            backgroundWorker1.DoWork += new DoWorkEventHandler(backgroundWorker1_DoWork);
            backgroundWorker1.RunWorkerCompleted += new RunWorkerCompletedEventHandler(backgroundWorker1_RunWorkerCompleted);
            backgroundWorker1.ProgressChanged += new ProgressChangedEventHandler(backgroundWorker1_ProgressChanged);
            
        }

        private void GoButton_Click(object sender, EventArgs e)
        {
            //var urlText = UrlInputTextBox.Text;
            //var arg = new DoWorkEventArgs(urlText);
            AutoFriendOutputTextBox.Text = "Working...";
            backgroundWorker1.WorkerSupportsCancellation = true;
            backgroundWorker1.RunWorkerAsync();
        }

        //D:\Data\WebContent\DaoDeJing.txt
        //D:\Data\WebContent\genesis.txt
        //http://www.foo.com/
        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            BackgroundWorker worker = sender as BackgroundWorker;
            var updater = new FormsUpdate(worker);

            var html = string.Empty;
            var input = UrlInputTextBox.Text;

            if (File.Exists(UrlInputTextBox.Text) && Path.GetExtension(UrlInputTextBox.Text) == ".txt")
            {
                try
                {
                    html = File.ReadAllText(UrlInputTextBox.Text);
                }
                catch (Exception ex)
                {
                    worker.ReportProgress(0, $"Failure: {ex.Message}");
                    e.Cancel = true;                }
            }
            else
            {
                try
                {
                    var webby = FriendlyWebManager.New(UrlInputTextBox.Text, updater);
                    html = webby.ReadWebsite();
                }
                catch (Exception ex)
                {
                    worker.ReportProgress(0, $"Failure: {ex.Message}");
                    e.Cancel = true;
                }
            }

            if (string.IsNullOrEmpty(html))
            {
                worker.CancelAsync();
                return;
            }


            output = html;

            var parser = FriendlyHtmlParser.New(html, updater);
           var result = parser.ReadLines();
            
            worker.ReportProgress(0, "Organizing...");
            var s = FriendlySorter.New(result);

            var lines = s.Sort();
            output = string.Join(Environment.NewLine, lines);

            worker.ReportProgress(0, output);

            var gram = FriendlyGrammarParser.New(lines.ToArray());
            var sentences = gram.CreateSentences();

            var memory = new MockMemory();
            var wrangler = ConceptWrangler.New(sentences, memory);
            wrangler.Consume();

            var god = memory.GetConcept("god");


        }

        // This event handler updates the progress.
        private void backgroundWorker1_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            //resultLabel.Text = (e.ProgressPercentage.ToString() + "%");
            var message = (string)e.UserState;
            AutoFriendOutputTextBox.Text = message;
        }

        private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            //AutoFriendOutputTextBox.Text = $"All done :-) We are here, damnit, we are here...   \r\n{AutoFriendOutputTextBox.Text}"; 

            if (e.Cancelled == true)
            {
               // resultLabel.Text = "Canceled!";
            }
            else if (e.Error != null)
            {
               // resultLabel.Text = "Error: " + e.Error.Message;
            }
            else
            {
              //  resultLabel.Text = "Done!";
            }

            AutoFriendOutputTextBox.Text = output;
        }

        private void TalkAboutButton_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(SubjectInputTextBox.Text))
                return;
            else if (string.IsNullOrWhiteSpace(SubjectInputTextBox.Text))
                return;

            var memory = new MockMemory();
            var speech = new BasicSpeech(memory);
            AutoFriendOutputTextBox.Text = speech.TalkAbout(SubjectInputTextBox.Text);
        }
    }
}
